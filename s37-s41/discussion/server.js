const express = require("express");
const mongoose = require('mongoose');
const usersRoutes = require('./routers/usersRoutes.js');
const coursesRoutes = require('./routers/coursesRoutes.js')
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const port = 4001;

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", 
	{ useNewUrlParser: true}, { userUnifiedTopology : true });
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));
db.once("open", ()=> console.log("Successfully connected to the cloud database!"));

app.use(bodyParser.urlencoded({ extended : false }))
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use('/users', usersRoutes);
app.use('/courses', coursesRoutes);


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
