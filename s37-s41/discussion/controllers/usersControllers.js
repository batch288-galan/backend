const Users = require('../models/Users.js');
const Courses = require('../models/Courses.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Controllers

module.exports.registerUser = (request, response) => {

		Users.findOne({ email : request.body.email })
		.then(result => {

			if(result) {
				return response.send(`${request.body.email} has been taken! Try logging in or use different email in signing up.`)
			} else {

				let newUser = new Users({
					firstName : request.body.firstName,
					lastName : request.body.lastName,
					email : request.body.email,
					password : bcrypt.hashSync(request.body.password, 10),
					isAdmin : request.body.isAdmin,
					mobileNo : request.body.mobileNo
				})

				newUser.save().then(saved => response.send(`${request.body.email} is now registered!`))
					.catch(error => response.send(error))

			}

		})
		.catch(error => response.send(error));

}

module.exports.loginUser = (request, response) => {

	Users.findOne({ email : request.body.email })
	.then(result => {
		if(!result) {
			return response.send(`${request.body.email} is not yet registred!`)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				/*return response.send("Login successfully!")*/

				return response.send({ auth : auth.createAccessToken(result) })

			} else {
				return response.send("Incorrect Password!")
			}
		}
	})
	.catch(error => response.send(error));

}

module.exports.getProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	// console.log(userData);

	if(userData.isAdmin){
	Users.findById(request.body.id)
		.then(result => {

			result.password = "confidential"
			return response.send(result)
		})
		.catch(error => response.send(error));
	} else {
		return response.send(`You are not an admin, you don't have access to this route!`);
	}

	
}


module.exports.enrollCourse = (request, response) => {

	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(`Admin cannot enroll a course!`)
	} else {
	 let isUserUpdated = Users.findOne({ _id : userData.id })
		.then(result => {
			result.enrollments.push({courseId : courseId})
			
			result.save()
			.then(saved => true)
			.catch(error => false)

		}) 
		.catch(error => false)

		let isCourseUpdated = Courses.findOne({_id : courseId})
		.then(result => {
			result.enrollees.push({ userId : userData.id })
			result.save()
			.then(saved => true)
			.catch(error => false)
		})
		.catch(error => false)

		if(isUserUpdated && isCourseUpdated){
			return response.send(`Enrollment is successful`)
		} else {
			return response.send(`There was an error in the enrollment, please try again!`)
		}

	}

}