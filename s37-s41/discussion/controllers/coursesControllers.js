const Courses = require('../models/Courses.js');
const auth = require('../auth.js');

module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		let newCourse = new Courses({
			name: request.body.name,
			description: request.body.desc,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots
		})

		newCourse.save()
		.then(save => response.send("Course successfully created!"))
		.catch(error => response.send(error));
	}else{
		return response.send("Unable to add course, You are not admin!")
	}

}

module.exports.getAllCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Courses.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	} else {
		return response.send(`You don't have access to this route!`)
	}

}


module.exports.getActiveCourses = (request, response) => {

	Courses.find({isActive : true})
	.then(result => response.send(result))
	.catch(error => response.send(error));

}

module.exports.getCourseDetails = (request, response) => {
	
	Courses.findById(request.params.courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error));

}

module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;
	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin) {
		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(`Successfully Updated`))
		.catch(error => response.send(error))
	} else {
		return response.send(`You don't have access to this route!`)
	}

}

module.exports.archiveCourse = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;
	const archivedCourse = {isActive: request.body.isActive}

	if(userData.isAdmin) {
		Courses.findByIdAndUpdate(courseId, archivedCourse)
		.then(result => response.send(`Course archived!`))
		.catch(err => response.send(err))
	} else {
		return response.send(`You don't have access to this route!`)
	}
}


module.exports.getInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin) {
		Courses.find({ isActive: false })
		.then(result => response.send(result))
		.catch(err => response.send(err))
	} else {
		return response.send(`You don't have access to this route`)
	}
}
