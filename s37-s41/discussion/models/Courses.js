const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required!"]
	},

	description: {
		type: String,
		required: [true, "Course description is required!"]
	},

	price: {
		type: Number,
		required: [true,"Course priceis required!"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date, 
		default: new Date()
	},

	slots: {
		type: Number,
		required: [true, "Course slots is required"]
	},

	enrollees: [
			{
				userId: {
					type: String,
					required: [true, "User ID of the enrollee is required"]
				},

				enrolledOn: {
					type: Date, 
					default: new Date()
				}
			}
		]
})

const Courses = mongoose.model("Course", courseSchema);

module.exports = Courses;