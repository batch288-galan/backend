const express = require('express');
const usersControllers = require ('../controllers/usersControllers.js');
const auth = require("../auth.js");

const router = express.Router();


router.post('/register', usersControllers.registerUser);
router.get("/login", usersControllers.loginUser);
router.get("/details", auth.verify, usersControllers.getProfile);
router.post('/enroll', auth.verify, usersControllers.enrollCourse)


module.exports = router; 