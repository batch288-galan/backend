const express = require('express');
const coursesControllers = require('../controllers/coursesControllers.js');
const auth = require('../auth.js');


const router = express.Router();

router.post('/add-course', auth.verify, coursesControllers.addCourse);

//route for retrieving all courses
router.get('/', auth.verify, coursesControllers.getAllCourses);


//route for retrieving all active courses
router.get('/active-courses', coursesControllers.getActiveCourses);

router.get('/inactive-courses', auth.verify, coursesControllers.getInactiveCourses)


//links with parameters
router.get('/:courseId', coursesControllers.getCourseDetails)

router.patch('/:courseId', auth.verify, coursesControllers.updateCourse)

router.patch('/:courseId/archive', auth.verify, coursesControllers.archiveCourse)




module.exports = router;