const http = require('http');

let directory = [
		{
			"name": "Brandon",
			"email": "brandon@gmail.com"
		},
		{
			"name": "Jobert",
			"email": "jobert@gmail.com"
		}
	]

const server = http.createServer((req, res) => {

	//GET METHOD	
	if (req.url == "/users" && req.method == "GET") {
		res.writeHead(200, {'content-type' : 'application/json'} );
		res.write(JSON.stringify(directory));
		res.end();
	}

	//POST METHOD
	if (req.url == "/addUser" && req.method == "POST") {
		
		let reqBody = "";

		req.on('data', (data) => {
			reqBody += data;
		});
		req.on('end', () => {

			reqBody = JSON.parse(reqBody);

			let newUser = {
				"name" : reqBody.name,
				"email" : reqBody.email
			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, { 'content-type': 'application/json' });
			res.write(JSON.stringify(newUser));
			res.end();

		});

	}

})

server.listen(8000, () => {
	console.log('Server is running on port 8000!')
})