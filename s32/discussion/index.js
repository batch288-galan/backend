const http = require('http');

const server = http.createServer((req, res) => {

	if (req.url == "/items" && req.method == "GET") {
		res.writeHead(200, {'content-type' : 'text-plain'} )
		res.end('Data retrieved from the database!')
	}

	if (req.url == "/items" && req.method == "POST") {
		res.writeHead(200, {'content-type' : 'text-plain'} )
		res.end('Data to be sent to the database!')
	}

})

server.listen(4000, () => {
	console.log('Server is running on port 4000!')
})