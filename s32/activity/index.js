const http = require('http');

const app = http.createServer((req, res) => {

    if (req.url == "/" && req.method == "GET") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end('Welcome to booking system')
    }

    else if (req.url == "/profile" && req.method == "GET") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end('Welcome to your profile!');
    }

    else if (req.url == "/courses" && req.method == "GET") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end("Here's our courses available!");
    }

    else if (req.url == "/addcourse" && req.method == "POST") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end('Add a course to our resources')
    }

    else if (req.url == "/updatecourse" && req.method == "PUT") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end('Update a course to our resources')
    }

    else if (req.url == "/archivecourses" && req.method == "POST") {
        res.writeHead(200, {'content-type' : 'text-plain'} )
        res.end('Archive a course to our resources')
    }

    else {
        res.writeHead(404, {'content-type' : 'text-plain'} )
        res.end('page cannot be found!');
    }


})
   

//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
