const express = require("express");

const app = express();

const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

	app.get("/", (request, response) => {
	
		response.send('Hello Batch 288!');
	})

	app.get("/hello", (request, response)=>{
		response.send("Hello from the /hello endpoint");
	})

	app.post("/hello", (request, response) => {
		console.log(request.body);

		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
	})

	let users = [];

	app.post("/signup", (request, response) => {
 		
 		if(request.body.username !== "" && request.body.password !== ""){

 			users.push(request.body);

 			response.send(`User ${request.body.username} successfully registered!`)

 		}else{
 			response.send('Please input BOTH username and password.');
 		}	
	})

	app.put("/change-password", (request, response) => {

		let message;

			for(let index = 0 ; index < users.length; index++){
				
				if(request.body.username == users[index].username){
						
					users[index].password = request.body.password

					message = `User ${request.body.username}'s password has been updated!`
					break;
				}else{
					message = 'User does not exist!'
				}

			}
			response.send(message);
	})


	app.get('/home', (request, response) => {
		response.send("Welcome to the homepage!");
	})

	app.get('/users', (request, response) => {
		response.send(users);
	})

	app.delete('/delete-user', (request, response) => {
		let message;

		if(users.length != 0) {
			for(let i = 0; i < users.length; i++ ) {
				if(request.body.username == users[index].username) {
					let removedUser = users.splice(0, 1);
					message = `User ${removedUser} has been deleted!`;
					break;
				}
			} 
		}

		if(users.length == 0) {
			message = `No users found!`
		}

		response.send(message);
		
	})


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
