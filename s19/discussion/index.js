let numH = 1;
if (numH > 2) {
	console.log("Hello");
}
else if (numH < 2) {
	console.log("World!");
}

let city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to new york!")
}
else if (city === "Tokyu") {
	console.log("Welcome to Tokyo!")
}

else console.log("Incorrect statements");

function checkTyphoonIntensity (windspeed) {
	if (windspeed < 0) {
		return "Invalid wind speed";
	}

	else if (windspeed <= 38 && windspeed > 0) {
		return "Tropical Depression";
	}

	else if (windspeed >= 39 && windspeed <= 73) {
		return "Tropical Storm";
	}

	else if (windspeed >= 74 && windspeed <= 95) {
		return "Signal # 1";
	}

	else if (windspeed >= 96 && windspeed <= 110) {
		return "Signal # 2";
	}

	else if (windspeed >=111  && windspeed <= 129) {
		return "Signal # 3";
	}

	else if (windspeed >= 130  && windspeed <= 156) {
		return "Signal # 4";
	}

	else {
		return "Signal # 5";	
	} 

}

console.log(checkTyphoonIntensity(30));

// console.warn() is a good way to print warnings in our console that could hel us developers act on a certain output within our.
console.warn(checkTyphoonIntensity(40));

// [Section] Truthy and Falsy
	// In JavaScript a truthy value is a value that is considered true when encounted in a boolean context.
	//Falsy values /excemption for truthy:
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5.null
		6. undefined
		7. NaN
	*/

	// Truthy Examples

	if(true){
		console.log("Truthy");
	}

	if(78){
		console.log("Truthy")
	}

	// Falsy Examples

	if(false){
		console.log("Falsy");
	}

	if(0 && 78){
		console.log("Falsy");
	}

// [Section] Contional (Ternary) Operator
	/*
	-The Ternary Operator takes in three operands:
		1.condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy

	-it can be used to an if else statement
	-Ternary Operators have an implicit return statement meaning without "return" the resulting expression can be stored in a variable.

		-Syntax:
			condition ? ifTrue : ifFalse;
	
	*/

// Single statement execution
let ternaryResult = (1 < 18) ? true : false;

console.log("Result of Ternary Operator: " + ternaryResult);

let exampleTernary = (0) ? "The number is not equal to zero" : "The number is equal to zero";

console.log(exampleTernary);

// Multiple Statement Execution
function isOfLegalAge(){
	let name = "John"
	return "You are in the legal age limit, " + name;
}

function isUnderAge(){
	let name = "John"
	return "You are under the legal age limit, " + name;
}

/*let age = parseInt(prompt("What is your age?"));
console.log(age);
console.log(typeof age);
let legalAge = (age > 18 ) ? isOfLegalAge() : isUnderAge();

console.log(legalAge);*/

// The parseInt function converts the input received as a string data type into number


//[Section] Switch Statements
/*
Syntax

switch(expression/variable) {
  case x:
    // code block
    break;
  case y:
    // code block
    break;
  default:
    // code block
    break;
}
*/

let day = prompt("What day of the week is today? ").toLowerCase();

switch(day) {
	case 'monday' :
		console.log("The color of the day is red!");
		break;
	case 'tuesday' :
		console.log("The color of the day is orange!");
		break;
	case 'wednesday' :
		console.log("The color of the day is yellow!");
		break;
	case 'thursday' :
		console.log("The color of the day is green!");
		break;
	case 'friday' :
		console.log("The color of the day is blue!");
		break;
	case 'saturday' :
		console.log("The color of the day is indigo!");
		break;
	case 'sunday' :
		console.log("The color of the day is violet!");
		break;
	default:
		console.log("Please input a valid day!");
		break;
}


// [Section] Try - Catch - Finally Statement

function showIntensityAlert(windspeed) {
	try{
		alert(checkTyphoonIntensity(windspeed))
	}
	catch(error) {
		console.log(typeof error)

		console.warn(error.message);
	}
	finally {
		//continue execution of code regardless of success and failure of code execution
		//in try block
		alert('Intensity updates will show new alert');
	}
}

showIntensityAlert(56);
console.log("Hi Im after the show intensity alert!");