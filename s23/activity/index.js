// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
// Initialize/add the given object properties and methods
// Properties
// Methods
// Check if all properties and methods were properly added
// Access object properties using dot notation
// Access object properties using square bracket notation
// Access the trainer "talk" method


let trainer = {
    name: "Ash Ketchum",
    age: 30,
    pokemon: ["Pikachu", "Charizard", "Garchomp", "Lucario", "Ditto"],
    friends: {
        firstName: 'Joe',
        lastName: 'Smith',
        address: {
            city: 'Austin',
            state: 'Texas' 
        },
        emails: ['joe@mail.com', 'joesmith@mail.xyz'],
    },
    talk: function(){
        console.log("Pikachu! I choose you!");
    }
}
console.log(trainer);
console.log('Result of talk method: ');
trainer.talk();



// Create a constructor function called Pokemon for creating a pokemon
// Create/instantiate a new pokemon
// Create/instantiate a new pokemon
// Create/instantiate a new pokemon
// Invoke the tackle method and target a different object
// Invoke the tackle method and target a different object

function Pokemon (name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level

    // Methods
    this.tackle = function(target){
        target.health = target.health - this.attack;
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to " + target.health)
        if(target.health <= 0) {
            return this.faint(target);
        }
    }
    this.faint = function(target){
        console.log(target.name + ' has fainted');
    }
}

// Create new instance of "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let eevee = new Pokemon("Eevee", 4);
let snorlax = new Pokemon("Snorlax", 90);






//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}