console.log("Hello World");

let firstName = "Lito", lastName = "Galan";
let fullName = firstName + " " + lastName;
	console.log("My full name is " + fullName);

let currentAge = 28;
	console.log("My current age is: " + currentAge);

let hobbies = ["Playing Guitar", "Online Gaming"];
	console.log("Hobbies: ");
	console.log(hobbies);

const workAddress = {
	houseNumber: 123,
	street: "Lagrimas",
	city: "Mondragon",
	state: "Northern Samar"
};

	console.log("Work Address: ")
	console.log(workAddress);


let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ");
	console.log(friends);

let profile = {
	username: "slash_009",
	fullName: fullName,
	age: currentAge,
	isActive: false,

};

	console.log("My Full Profile: " )
	console.log(profile);


let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

const lastLocation = "Arctic Ocean";

//cannot reaasign constant values
//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
	try{
		module.exports = {
		    firstName: typeof firstName !== 'undefined' ? firstName : null,
		    lastName: typeof lastName !== 'undefined' ? lastName : null,
		    age: typeof age !== 'undefined' ? age : null,
		    hobbies: typeof hobbies !== 'undefined' ? hobbies : null,
		    workAddress: typeof workAddress !== 'undefined' ? workAddress : null,
		    fullName: typeof fullName !== 'undefined' ? fullName : null,
		    currentAge: typeof currentAge !== 'undefined' ? currentAge : null,
		    friends: typeof friends !== 'undefined' ? friends : null,
		    profile: typeof profile !== 'undefined' ? profile : null,
		    fullName2: typeof fullName2 !== 'undefined' ? fullName2 : null,
		    lastLocation: typeof lastLocation !== 'undefined' ? lastLocation : null
		}
	} catch(err){
}