// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = "";


// console.log(string);
// let vowels = /[aeiou]/ig;
// let arr2 = string.match(vowels);
// let filteredString = [];

// for (let i = 0 ; i < string.length ; i++){
//     if(arr2.indexOf(string[i]) === -1)
//     {
//         filteredString.push(string[i])
//     }
// }
// console.log(filteredString.join(""));

//add code here
for (index = 0; index < string.length; index++) {
    if(string[index] === "a" ||
        string[index] === "e" ||
        string[index] === "i" ||
        string[index] === "o" ||
        string[index] === "u"){

        continue;
    } else {
        filteredString += string[index];
    }
}

console.log(filteredString);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}