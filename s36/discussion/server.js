const express = require("express");
const mongoose = require('mongoose');
const taskRoutes = require('./routers/taskRoutes.js');

const app = express();
const port = 5000;

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/batch288-todo?retryWrites=true&w=majority", 
	{ useNewUrlParser: true });
let db = mongoose.connection;

db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));
db.once("open", ()=> console.log("Successfully connected to the cloud database!"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/tasks", taskRoutes);


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
