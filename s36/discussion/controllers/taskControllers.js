const Task = require("../models/task.js");

module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error);
	})
}

module.exports.addTasks = (request, response) => {
	Task.findOne({"name" : request.body.name})
	.then(result => {
		if(result !== null) {
			return response.send('Task already exists!')
		} else {
			let newTask = new Task({
				"name": request.body.name 
			})
			newTask.save();
			return response.send("New task created!")
		}
	}).catch(error => response.send(error))
}

module.exports.deleteTask = (request, response) => {
	 let taskToBeDeleted = request.params.id;
	 Task.findByIdAndRemove(taskToBeDeleted)
	 .then(result => {
	 	return response.send(`The document ${taskToBeDeleted} has been deleted`)
	 }).catch(error => response.send(error));
}

module.exports.getTask = (request, response) => {
	Task.findById(request.params.id)
	.then(result => {
		return response.send(result);
	})	
}

module.exports.updateTask = (request, response) => {
	Task.findByIdAndUpdate(request.params.id, {
		$set : {
			new: true,
			status: "complete"
		}
	})
	.then(result=> {
		return response.send(result);
	})
	.catch(error => response.send(error));
}