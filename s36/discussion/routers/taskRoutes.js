const express = require("express");
const taskControllers = require("../controllers/taskControllers.js");

const router = express.Router();

router.get("/", taskControllers.getAllTasks);
router.post("/addTask", taskControllers.addTasks);

router.delete("/:id", taskControllers.deleteTask);
router.get("/:id", taskControllers.getTask);
router.put("/:id/complete",taskControllers.updateTask);

module.exports = router;