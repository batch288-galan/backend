// console.log("Hello Batch 288!");

//[Section] JavaScript Synchronous vs Asynchronous
	// Javascript is  by default is synchronous meaning that only one statement is executed a time.
	
	// This can be proven when a statement has an error, javascript will not proceed with the next statement
/*console.log("Hello World!");

conole.log("Hello after the world");

console.log("Hello");*/

	//When certain statements take a lot of time to process, this slows down our code.

	// for(let index = 0; index <= 100000; index++){
	// 	console.log(index);
	// }

	// console.log("Hello again!");

	//Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

//[Section] Getting all posts
	//The Fetch API allows you to asynchronously request for a resource data.
	//so it means the fetch method that we are going to use here will run asynchronously.
	//Syntax:
		//fetch('URL');
	
	//A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	//Syntax:
		/*
			fetch('URL')
			.then((response => response));
		*/
	//REtrive all posts follow the REST API

	fetch('https://jsonplaceholder.typicode.com/posts')
	//THe fetch method will return a promise that resolves the Response object.

	//the "then" method captures the response object
	//Use the "json" method from the Response object to convert the data retrived into JSON format to be used in our application
	.then(response => response.json())
	.then(trial => console.log(trial))
	

	//The "async" and "await" keyword, it is another approach that can be used to achieve asynchonous code

	//Creates an asynchronous function

	async function fetchData(){

		let result = await fetch('https://jsonplaceholder.typicode.com/posts');

		console.log(result);

		let json = await result.json();

		console.log(json);

	};

	// fetchData();

	//[Section] Getting specific post

	//Retrieves specific post following the rest API(/posts/:id)

	fetch('https://jsonplaceholder.typicode.com/posts/5')
	.then(response => response.json())
	.then(json => console.log(json));

	//[Section] Creating Post
	//Syntax: 
		/*
			//options is an object that contains the method, the header and the body of the request

			//by default if you don't add the method in the fetch request, it will be a GET method.

			fetch('URL', options )
			.then(response => {})
			.then(response => {})
	
		*/
	fetch('https://jsonplaceholder.typicode.com/posts', {
		//sets the method of the rquest object to post following the rest API
		method: 'POST',
		//sets the header data of the request object to be sent to the backend
		//specified that the content will be in JSON structure
		headers: {
			'Content-type':'application/json'
		},
		//sets the content/nody data of the request object to be sent backend
		body: JSON.stringify({
					title : 'New post',
					
				})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	//[Section] Update a specific Post

	//PUT method
	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: "PUT",
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({	
					title: 'Updated Post',				
				})
	})
	.then(response => response.json())
	.then(json => console.log(json))


	//PATCH
	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: "PATCH",
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
					title: 'Updated Post'
				})
	})
	.then(response => response.json())
	.then(json => console.log(json))

	//The PUT method is a method of modifying resource where the client sends data that upadtes the entire object/document

	//Patch method applies a partial update to the object or document.

	//[Section] Deleting a post

	//deleting specific post followin the REST API
	fetch('https://jsonplaceholder.typicode.com/posts/1', {method : "DELETE"})
	.then(response => response.json())
	.then(json => {
		//map method
	});






