const express = require('express');
const mongoose = require('mongoose');

const port = 3001;
const app = express();

	mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/batch288-todo?retryWrites=true&w=majority", 
		{ useNewUrlParser: true });
	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));
	db.once("open", ()=> console.log("We're connected to the cloud database!"));

	app.use(express.json());
	app.use(express.urlencoded({extended:true}))

	const taskSchema = new mongoose.Schema({
	
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	})

	const Task = mongoose.model('Task', taskSchema)

	app.post("/tasks", (request, response) => {
		console.log(request.body);
		Task.findOne({name: request.body.name})
		.then(result => {
			if(result !== null){
				return response.send("Duplicate task found!");
			}else{

				let newTask = new Task({
					name: request.body.name
				})

				newTask.save()

				return response.send('New task creted!')
			}


		})
	})

	app.get("/tasks", (request, response) => {
		Task.find({})
		.then(result =>{
			return response.send(result);
		})
		.catch(error => response.send(error));

	});

	//ACTIVITY
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	});

	const User = mongoose.model('User', userSchema);

	app.post('/signup', (request, response) => {

		User.findOne({username: request.body.username})
		.then(result => {
			if(result !== null){
				return response.send("Duplicate username found!");

			} else {
				if(request.body.username !== "" && request.body.password !== "") {
					let newUser = new User ({
						username: request.body.username,
						password: request.body.password
					})

					newUser.save();
					return response.send('New user registered!')
				} else {
					return response.send('BOTH username and password must be provided!')
				}

			}
		})		

		/*let newUser = new User ({
			username: request.body.username,
			password: request.body.password
		});

		newUser.save();
		return response.send('New user added!')*/

		
	})


if(require.main === module){
	app.listen(port, ()=> {
		console.log(`Server is running at port ${port}!`)
	})	
}

module.exports = app;
